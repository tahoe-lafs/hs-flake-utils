{
  description = "a Nix flake library for Haskell projects";

  inputs = {
    # The following hash is what obsidian use:
    # https://github.com/reflex-frp/reflex-platform/blob/2e0d31bc1b565eed02a3848cde0e0dbc4f33a2d4/nixpkgs/github.json
    nixpkgs.url = "github:obsidiansystems/nixpkgs?rev=22cf53eb29f52af963398de744c422d6fe15a3d4";
    flake-utils.url = "github:numtide/flake-utils";
    flake-utils.inputs.nixpkgs.follows = "nixpkgs";
    pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";
    pre-commit-hooks.inputs.nixpkgs.follows = "nixpkgs";
    pre-commit-hooks.inputs.flake-utils.follows = "flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    pre-commit-hooks,
    flake-utils,
  }: {
    lib = import ./lib.nix {
      inherit self nixpkgs pre-commit-hooks flake-utils;
    };
  };
}
