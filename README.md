# hs-flake-utils

A nix flake for use with Haskell, for Least Authority.

# How do I make it go?

1. create a directory
   change into that directory
2. nix shell nixpkgs#cabal-install nixpkgs#ghc
   cabal init and answer all the questions to create the project files
3. copy the project template below into a file named `flake.nix`
   replace the contents of `description` and `packageName` in the newly created `flake.nix`
4. run `nix develop`
   Wait for the world to build or the internet to send you cached packages.

```nix
{
  description = "something about your cool project";

  inputs = {
	# Nix Inputs
	nixpkgs.url = github:nixos/nixpkgs/?ref=nixos-22.11;
	flake-utils.url = github:numtide/flake-utils;
	hs-flake-utils.url = "git+https://whetstone.private.storage/jcalderone/hs-flake-utils.git?ref=main";
	hs-flake-utils.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {
	self,
	nixpkgs,
	flake-utils,
	hs-flake-utils,
  }: let
	ulib = flake-utils.lib;
	ghcVersion = "ghc8107";
  in
	ulib.eachSystem ["x86_64-linux" "aarch64-darwin"] (system: let
	  # Get a nixpkgs customized for this system
	  pkgs = import nixpkgs {
		inherit system;
	  };
	  hslib = hs-flake-utils.lib {
		inherit pkgs;
		src = ./.;
		compilerVersion = ghcVersion;
		packageName = "PACKAGENAME";
	  };
	in {
	  checks = hslib.checks {};
	  devShells = hslib.devShells {
		extraBuildInputs = pkgs:
		  with pkgs; [
			 zlib
		  ];
	  };
	  packages = hslib.packages {};
	  apps.release = hslib.apps.release { };
	});
}
```
